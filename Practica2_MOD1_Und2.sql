﻿DROP DATABASE IF EXISTS practica2mod1; 
CREATE DATABASE practica2mod1;
USE practica2mod1;

CREATE TABLE coches (
id int AUTO_INCREMENT,
marca varchar (100),
fecha date,
precio float,
PRIMARY KEY (id)
);

CREATE TABLE clientes (
cod int AUTO_INCREMENT,
nombre varchar(100),
PRIMARY KEY (cod)
);

CREATE TABLE comprados (
idcoches int ,
codclientes int AUTO_INCREMENT,
PRIMARY KEY (idcoches, codclientes),
  UNIQUE KEY (idcoches),
  CONSTRAINT fkcompradoscoches FOREIGN KEY (idcoches) REFERENCES coches (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fkcompradosclientes FOREIGN KEY (codclientes) REFERENCES clientes (cod) ON DELETE RESTRICT ON UPDATE RESTRICT
);

-- introducion de Datos 

  INSERT INTO coches 
  (id, marca, fecha, precio)        VALUES 
  (1,'Renault',  '2021/03/27', 3000),
  (2,'Kia',      '2021/04/17', 4000),
  (3,'Toyota',   '2020/12/23', 6000),
  (4,'Volkswagen', '2021/06/22', 5500) ;

INSERT INTO clientes 
  (cod, nombre)   VALUES 
  (1, 'Marcos Perez'),
  (2, 'Maria OViedo'),
  (3, 'Raul Hernandez'),
  (4, 'Martin Otero');

INSERT INTO comprados 
  (idcoches, codclientes)  VALUES 
   (1, 1),
   (2, 2),
   (3, 2),
   (4, 4);  






  SELECT * FROM coches c;
  SELECT * FROM clientes c;
  SELECT * FROM comprados c;



